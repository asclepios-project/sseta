# Asclepios-TrustedAuthority

SSE Trusted Authority (SSE TA) is a main component for SSE. It stores the metadata which is used for generating the search/update/delete token (at the SSE client), and verifying the search/update/delete tokens on behalf of the SSE server.

The details about how to run SSE TA in docker container could be found in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf). Apart from that, the details about how to run SSE TA using MiCADO could also be reached in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf).
